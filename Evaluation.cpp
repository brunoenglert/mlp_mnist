#include "Evaluation.h"

arma::umat Evaluation::oneHot(arma::Mat<float> input, double tolerance) {
    return tolerance <= input;
}

int Evaluation::oneHotToNumber(arma::Row<float> input, double tolerance) {
    arma::uvec result = arma::find(input >= tolerance);
    return result(0);
}

double Evaluation::validationError(arma::Mat<float> input, arma::Mat<float> output) {
    double error = arma::accu<arma::mat>(arma::square<arma::mat>(arma::conv_to<arma::mat>::from(input - output)) / (double) input.n_rows);
    return error;
}

double Evaluation::accuracy(arma::Mat<float> input, arma::Mat<float> output, double tolerance) {
    arma::umat predictions = oneHot(input, tolerance);
    unsigned int correct = 0;

    for (int r = 0; r < input.n_rows; r++) {

        if (arma::sum(predictions.row(r) - output.row(r)) == 0) {
            correct += 1;
        }
    }
    return (double) correct / input.n_rows;
}

arma::Mat<double> Evaluation::confusionMatrix(arma::Mat<float> input, arma::Mat<float> output, double tolerance) {
    arma::Mat<double> predictions = arma::conv_to<arma::mat>::from(oneHot(input, tolerance));
    //arma::uvec expectedNumbers =

    int nbClasses = output.n_cols;
    arma::Mat<double> result = arma::zeros<arma::mat> (nbClasses, nbClasses);
    //
    for (int r = 0; r < input.n_rows; r++) {
        int idx = oneHotToNumber(output.row(r), 0.99);
        result.row(idx) += predictions.row(r);
    }
    return result;
}