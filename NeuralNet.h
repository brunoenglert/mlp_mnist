#ifndef NEURALNET_H
#define NEURALNET_H
#include <iostream>
#include <vector>
#include <armadillo>
#include "functions.h"
#include "Evaluation.h"
using namespace std;

#define EPSILON 0.2

class NeuralNet {
private:
    std::vector<int> layerStructure;
    std::vector<FunctionType> activationStructure;
    std::vector<arma::Mat<float> > layers;
    std::vector<arma::Mat<float> > weights;
    std::vector<arma::Mat<float> > bias;

    std::vector<arma::Mat<float> > deltaLayers;
    std::vector<arma::Mat<float> > deltaWeights;

    arma::uvec indicies;
    float learningRate;
    unsigned int batchSize;
    unsigned int nbSamples;
    unsigned int nbTrainSamples;
    unsigned int nbValidationSamples;
public:

    NeuralNet() {
    }

    void init() {

        for (unsigned int i = 0; i < layerStructure.size(); i = i + 1) {
            addInitMatrix(this->layers, batchSize, layerStructure.at(i));
            addInitMatrix(this->deltaLayers, batchSize, layerStructure.at(i));
        }

        for (unsigned int i = 0; i < layerStructure.size() - 1; i = i + 1) {
            addInitMatrix(this->weights, layerStructure.at(i), layerStructure.at(i + 1));
            addInitMatrix(this->deltaWeights, layerStructure.at(i), layerStructure.at(i + 1));
            addInitMatrix(this->bias, 1, layerStructure.at(i + 1));
        }

        for (int i = 0; i < weights.size(); i++) {
            cout << "layer" << i << ":   "
                    << "weight size:" << arma::size(weights[i]) << "    "
                    << "bias size:" << arma::size(bias[i]) << endl;

        }
    }

    void addInitMatrix(std::vector<arma::Mat<float> > &mat, int rows, int cols) {
        mat.push_back(randWeights(rows, cols));
    }

    arma::Mat<float> forwardPropagation(arma::Mat<float> input);
    void backPropagation(arma::Mat<float> output);
    void train(arma::Mat<float>& input, arma::Mat<float>& output, unsigned int epoch, unsigned int batchSize, float learningRate, float validationRatio, float noise, bool saveFlag);
    void test(arma::Mat<float>& input, arma::Mat<float>& output);
    void addLayer(int neuronCount, std::string type);
    void addInput(int neuronCount);
    arma::Mat<float> randWeights(int rows, int cols);
    void activation(arma::Mat<float>& input, FunctionType type);
    arma::Mat<float> deactivation(arma::Mat<float> input, FunctionType type);
    bool save();
    bool load();
};


#endif /* NEURALNET_H */

