#ifndef MNISTREADER_H
#define MNISTREADER_H

#include <stdexcept>
#include <cstdlib>
#include <armadillo>

class MnistReader {
public:
    static arma::fmat readImages(std::string filename);
    static arma::fmat readLabels(std::string filename);
};

#endif /* MNISTREADER_H */

