#include <sstream>
#include "NeuralNet.h"
#include "progressbar.h"


using namespace std;
using namespace arma;

void NeuralNet::train(arma::Mat<float>& input, arma::Mat<float>& output, unsigned int epoch, unsigned int batchSize, float learningRate, float validationRatio, float noise, bool saveFlag) {
    this->learningRate = learningRate;
    this->batchSize = batchSize;
    this->nbSamples = input.n_rows;
    this->nbTrainSamples = (int) this->nbSamples * validationRatio;
    this->nbValidationSamples = this->nbSamples - this->nbTrainSamples;
    this->indicies = arma::linspace<arma::uvec>(0, this->nbTrainSamples - 1, this->nbTrainSamples);

    for (int e = 0; e < epoch; e++) {
        cout << "epoch: " << e << " ---------------" << " learningRate: " << this->learningRate << endl;
        this->indicies = arma::shuffle(this->indicies);
        for (int b = 0; b < nbTrainSamples; b += this->batchSize) {
            progressBar((double) b / nbTrainSamples, 70);
            if (noise > 0) {
                Mat<float> noiseMat = arma::randu<arma::fmat>(batchSize, input.n_cols)*2 * noise - noise;
                forwardPropagation(input.rows(this->indicies.rows(b, b + this->batchSize - 1)) + noiseMat);
            } else {
                forwardPropagation(input.rows(this->indicies.rows(b, b + this->batchSize - 1)));
            }
            backPropagation(output.rows(this->indicies.rows(b, b + this->batchSize - 1)));
            //return;
        }

        if (0 < nbValidationSamples) {
            arma::Mat<float> validationInput = forwardPropagation(input.rows(nbTrainSamples, nbSamples - 1));
            arma::Mat<float> validationOutput = output.rows(nbTrainSamples, nbSamples - 1);

            cout << endl << "error:" << Evaluation::validationError(validationInput, validationOutput) << "   "
                    << "accuracy: " << Evaluation::accuracy(validationInput, validationOutput, 0.8) << endl;
        }
        if (saveFlag) {
            save();
        }

        this->learningRate *= 0.90;
    }

}

inline arma::Mat<float> NeuralNet::forwardPropagation(arma::Mat<float> input) {
    int size = layerStructure.size();

    layers[0] = input;
    for (int i = 0; i < size - 1; i++) {
        layers[i + 1] = (layers[i] * weights[i]);
        layers[i + 1].each_row() += bias[i];
        activation(layers[i + 1], activationStructure[i + 1]);
    }
    return layers.back();
}

inline void NeuralNet::backPropagation(arma::Mat<float> output) {
    int size = layerStructure.size();

    deltaLayers[size - 1] = (layers[size - 1] - output) * 2 * learningRate;

    for (int i = size - 2; 0 <= i; i--) {
        deltaLayers[i] = deltaLayers[i + 1] * weights[i].t() % deactivation(layers[i], activationStructure[i]);
        deltaWeights[i] = layers[i].t() * deltaLayers[i + 1] / this->batchSize;
    }
    for (int j = 0; j < weights.size(); j++) {
        weights[j] -= deltaWeights[j];
        bias[j] -= sum(deltaLayers[j + 1], 0) / this->batchSize;

    }
}

void NeuralNet::test(arma::Mat<float>& input, arma::Mat<float>& output) {
    arma::Mat<float> predicted = forwardPropagation(input);

    cout << endl << "error:" << Evaluation::validationError(predicted, output) << "   "
            << "accuracy: " << Evaluation::accuracy(predicted, output, 0.8) << endl;
    cout << "confusion matrix: " << endl << Evaluation::confusionMatrix(predicted, output, 0.8) << endl;
}

bool NeuralNet::save() {
    std::ostringstream oss;

    for (int j = 0; j < weights.size(); j++) {
        oss.str("");
        oss << "layer" << j << "_weight" << arma::size(weights[j]) << ".mat";
        if (!weights[j].save(oss.str()))
            return false;
        oss.str("");
        oss << "layer" << j << "_bias" << arma::size(bias[j]) << ".mat";
        if (!bias[j].save(oss.str()))
            return false;
    }
    cout << " finsihed saving" << endl;

    return true;
}

bool NeuralNet::load() {
    std::ostringstream oss;

    for (int j = 0; j < weights.size(); j++) {
        oss.str("");
        oss << "layer" << j << "_weight" << arma::size(weights[j]) << ".mat";
        if (!weights[j].load(oss.str()))
            return false;
        oss.str("");
        oss << "layer" << j << "_bias" << arma::size(bias[j]) << ".mat";
        if (!bias[j].load(oss.str()))
            return false;
    }
    cout << " finsihed loading" << endl;

    return true;
}

arma::Mat<float> NeuralNet::randWeights(int rows, int cols) {
    return arma::randu<arma::fmat>(rows, cols)*2 * EPSILON - EPSILON;
}

inline void NeuralNet::activation(arma::Mat<float>& input, FunctionType type) {
    switch (type) {
        case FunctionType::relu:
            ReLU(input);
            break;
        case FunctionType::sigmoid:
            Sigmoid(input);
            break;
        case FunctionType::linear:
            Linear(input);
            break;
        default:
            throw std::invalid_argument("invalid activation function");
    }

}

inline arma::Mat<float> NeuralNet::deactivation(arma::Mat<float> input, FunctionType type) {
    switch (type) {
        case FunctionType::relu:
            return DeReLU(input);
        case FunctionType::sigmoid:
            return DeSigmoid(input);
        case FunctionType::linear:
            return DeReLU(input);
        default:
            throw std::invalid_argument("invalid de-activation function");
    }
}

void NeuralNet::addLayer(int neuronCount, std::string activationType) {
    this->layerStructure.push_back(neuronCount);
    this->activationStructure.push_back(getFunctionType(activationType));
}

void NeuralNet::addInput(int neuronCount) {
    this->layerStructure.push_back(neuronCount);
}



