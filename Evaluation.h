#ifndef EVALUATION_H
#define EVALUATION_H

#include <armadillo>

class Evaluation {
public:
    static arma::umat oneHot(arma::Mat<float> input, double tolerance);

    static int oneHotToNumber(arma::Row<float> input, double tolerance);

    static double validationError(arma::Mat<float> input, arma::Mat<float> output);

    static double accuracy(arma::Mat<float> input, arma::Mat<float> output, double tolerance);

    static arma::Mat<double> confusionMatrix(arma::Mat<float> input, arma::Mat<float> output, double tolerance);


};
#endif /* EVALUATION_H */

