#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <armadillo>
#include <math.h>

enum struct FunctionType {
    relu, linear, sigmoid
};

inline void ReLU(arma::Mat<float> &input) {
    arma::fmat::iterator it_end = input.end();

    for (arma::fmat::iterator it = input.begin(); it != it_end; ++it)
        if (*it < 0) {

            *it = 0;
        }

}

inline arma::Mat<float> DeReLU(arma::Mat<float> input) {
    arma::fmat::iterator it_end = input.end();

    for (arma::fmat::iterator it = input.begin(); it != it_end; ++it)
        if (*it <= 0) {
            *it = 0;
        } else {

            *it = 1;
        }
    return input;
}

inline float Sigmoid(float x) {
    return 1 / (1 + exp(-x));
}

inline void Sigmoid(arma::Mat<float> &input) {
    arma::fmat::iterator it_end = input.end();

    for (arma::fmat::iterator it = input.begin(); it != it_end; ++it)
        *it = Sigmoid(*it);

}

inline arma::Mat<float> DeSigmoid(arma::Mat<float> input) {
    arma::fmat::iterator it_end = input.end();

    for (arma::fmat::iterator it = input.begin(); it != it_end; ++it)
        *it = Sigmoid(*it) * Sigmoid(1 - *it);
    return input;
}

inline void Linear(arma::Mat<float> &input) {

}

inline arma::Mat<float> DeLinear(arma::Mat<float> input) {
    arma::fmat::iterator it_end = input.end();

    for (arma::fmat::iterator it = input.begin(); it != it_end; ++it)
        *it = 1;
    return input;
}

static FunctionType getFunctionType(std::string& inString) {
    std::transform(inString.begin(), inString.end(), inString.begin(), ::tolower);
    if (inString == "relu") return FunctionType::relu;
    if (inString == "sigmoid") return FunctionType::sigmoid;
    if (inString == "linear") return FunctionType::linear;
}


#endif /* FUNCTIONS_H */

