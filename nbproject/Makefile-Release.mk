#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/ebcce8c0/Evaluation.o \
	${OBJECTDIR}/_ext/ebcce8c0/MnistReader.o \
	${OBJECTDIR}/_ext/ebcce8c0/NeuralNet.o \
	${OBJECTDIR}/_ext/ebcce8c0/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mlp_mnist

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mlp_mnist: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mlp_mnist ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/ebcce8c0/Evaluation.o: /home/bence/work/mlp_mnist/Evaluation.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/ebcce8c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ebcce8c0/Evaluation.o /home/bence/work/mlp_mnist/Evaluation.cpp

${OBJECTDIR}/_ext/ebcce8c0/MnistReader.o: /home/bence/work/mlp_mnist/MnistReader.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/ebcce8c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ebcce8c0/MnistReader.o /home/bence/work/mlp_mnist/MnistReader.cpp

${OBJECTDIR}/_ext/ebcce8c0/NeuralNet.o: /home/bence/work/mlp_mnist/NeuralNet.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/ebcce8c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ebcce8c0/NeuralNet.o /home/bence/work/mlp_mnist/NeuralNet.cpp

${OBJECTDIR}/_ext/ebcce8c0/main.o: /home/bence/work/mlp_mnist/main.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/ebcce8c0
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/ebcce8c0/main.o /home/bence/work/mlp_mnist/main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mlp_mnist

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
