/*
 * File:   main.cpp
 * Author: bruno englert
 *
 * Created on 2017. június 26., 19:01
 */
#include <stdexcept>
#include <iostream>
#include <cstdlib>
#include <fstream>
#include "json.hpp"
#include <armadillo>
#include "NeuralNet.h"
#include "MnistReader.h"
#include "Evaluation.h"

using namespace std;
using json = nlohmann::json;

int main(int argc, char** argv) {
    try {
        // read a JSON file
        std::ifstream i("parameters.json");
        json j;
        i >> j;

        int epoch = j["epoch"];
        int batchSize = j["batch_size"];
        float learningRate = j["learning_rate"];
        float validationRatio = j["validation_ratio"];
        float noise = j["noise"];
        bool saveWeights = j["save_weights"];


        /* loading images */
        arma::fmat images = MnistReader::readImages(j["filename_images"]);
        arma::fmat labels = MnistReader::readLabels(j["filename_labels"]);
        /* loading test images */
        arma::fmat test_images = MnistReader::readImages(j["filename_images_test"]);
        arma::fmat test_labels = MnistReader::readLabels(j["filename_labels_test"]);

        /*normalization*/
        arma::Row<float> avrImages = arma::mean(images, 0);
        arma::Row<float> stdImages = arma::stddev(images, 0, 0);
        //  images.each_row() -= avrImages;
        //  images.each_row() /= stdImages;
        /*  */
        images /= 255;
        test_images /= 255;

        NeuralNet neuralNet = NeuralNet();

        for (int i = 0; i < j["layer_size"].size(); i++) {
            neuralNet.addLayer(j["layer_size"][i], j["layer_activation"][i]);
        }
        neuralNet.init();

        if (j["load_weights"]) {
            if (neuralNet.load() == false) {
                cout << "couldn't load matrices" << endl;
            }
        }
        if (j["train"]) {
            neuralNet.train(images, labels, epoch, batchSize, learningRate, validationRatio, noise, saveWeights);
        }
        if (j["test"]) {
            neuralNet.test(test_images, test_labels);
        }
        if (j["predict"]) {
            for (int r = 0; r < images.n_rows; r++) {
                arma::Mat<float> predictions = neuralNet.forwardPropagation(images);
                cout << Evaluation::oneHotToNumber(predictions.row(r), 0.8) << endl;
            }
        }

    } catch (const std::exception& e) {
        cout << e.what() << endl;

    }
    return 0;
}

